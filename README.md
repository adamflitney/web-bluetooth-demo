# web-bluetooth-test
A quick demo of using web bluetooth to turn on/off an LED on a Genuino 101



A great resource for how to use web bluetooth by the way: https://developers.google.com/web/updates/2015/07/interact-with-ble-devices-on-the-web

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```
